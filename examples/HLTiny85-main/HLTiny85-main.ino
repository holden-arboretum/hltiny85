#include <avr/sleep.h>
#include <HLTiny85.h>
#define adc_disable() (ADCSRA &= ~(1<<ADEN))

HLTiny85 tiny;

void setup() {

  adc_disable(); //Disable the analog to digital converter to save battery
  
  //Power down the microprocessor, will awaken to perform commands when pin change interrupt is flagged
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  sleep_enable();
  sleep_cpu();

  
  
}

void loop() {
  
}

ISR(USI_OVF_vect) {
  tiny.do_overflow_interrupt();
}

ISR(PCINT0_vect) {
 tiny.do_pin_change_interrupt();
 
}
