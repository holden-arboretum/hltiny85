#include "HLTiny85.h"
#include <avr/io.h>
#include <avr/interrupt.h>

#define GET_LIGHT 0x01
#define EEPROM_WRITE 0x02
#define EEPROM_READ 0x03
#define EEPROM_ADDRESS 0x10
#define SPI_TEST 0xFF

HLTiny85::HLTiny85() {
  // disable interrupts
  cli();

  // set data direction for MISO to output
  // All others will be inputs
  DDRB = (1<<DDB1);
  

  // set our wire mode to three wire for SPI
  // Set clock source to external, positive edge
  USICR = (1<<USIWM0)|(1<<USICS1);
  // Initialize USI Overflow Interrupt
  USICR &= ~(1<<USIOIE);
  
  
 //Enable ADC
  // Select PB4 as our analog input
  ADCSRA = (1<<ADEN)|(1<<ADPS1)|(0<<ADPS0);
  
  
  // enable pin change interrupts on PB3
  GIMSK |= (1<<PCIE);
  PCMSK |= (1<<PCINT3);
  sei();

  // initialize our variables
  got_eeprom_write = false;
  pos = 0;
}

void HLTiny85::write_calibration(uint8_t data) {
  // disable interrupts to avoid EEPROM corruption
  cli();

  // wait for previous EEPROM write if exists
  while (EECR & (1<<EEPE));

  // set programming mode to erase value and then write new value
  EECR = (0<<EEPM1)|(0<<EEPM0);

  // Set our address and data
  EEAR = EEPROM_ADDRESS;
  EEDR = data;

  /* Write logical one to EEMPE in order to make writing a logical
   * one to EEPE have an effect */
  EECR |= (1<<EEMPE);

  // Start eeprom write by setting EEPE, EEPE automatically resets to zero
  EECR |= (1<<EEPE);

  // re-enable global interrupts
  sei();
}

uint8_t HLTiny85::read_calibration() {
  // wait for previous EEPROM write if exists
  while (EECR & (1<<EEPE));

  // set our address
  EEAR = EEPROM_ADDRESS;

  // do our read
  EECR |= (1<<EERE);

  return EEDR;
}

union Float HLTiny85::analog_read() {
  
  

  // Select PB4 as our analog input
  ADMUX = (1<<MUX1);

  union Float adc_avg;

  int i;
  for (i = 10; i > 0; i--) {
    // start analog read
    ADCSRA |= (1<<ADSC);

    // wait for analog read to complete
    while (ADCSRA & (1<<ADSC));

    uint8_t low = ADCL;
    uint8_t high = ADCH;

    adc_avg.val += (high<<8) | low;
  }

  //Disable the ADC
  //ADCSRA &= ~(1<<ADEN);
  
  
  adc_avg.val /= 10;
  return adc_avg;
}

void HLTiny85::do_overflow_interrupt() {
  // get data from the buffer
  uint8_t command = USIDR;

  if (got_eeprom_write) {
    got_eeprom_write = false;
    write_calibration(command);
  }

  switch (command) {
    case GET_LIGHT:
        if (pos == 0) {
          previous_read = analog_read();
        }
        if (pos != 4) {
          USIDR = previous_read.bytes[pos];
        }
        pos++;
        if (pos > 4) {
          pos = 0;
        }
      break;
    case EEPROM_WRITE:
      got_eeprom_write = true;
      break;
    case EEPROM_READ:
      USIDR = read_calibration();
      break;
    case SPI_TEST:
      USIDR = 0x10;
      break;
  }

  // reset the interrupt flags
  USISR = (1<<USIOIF);
  sei();
}

void HLTiny85::do_pin_change_interrupt() {

  // check if the pin is low or high
  uint8_t pin_state = PINB & (1 << PB3);

  // if its low, enable interrupts
  if (!pin_state) {
    // Enable USI overflow interrupt
    USICR |= (1<<USIOIE);
    sei();
  } else {
    // Disable USI overflow interrupt
    USICR &= ~(1<<USIOIE);
    sei();
  }


  

}


