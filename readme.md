# HLTiny85

A library written for the ATtiny85 running on the custom Holden Arboretum optical sensor board

## Programming

Programming the ATtiny85 with this library is accomplished via the Arduino IDE. There should be a little red programmer included in the box given to you by Larry. This is a tiny AVR programmer which makes programming the chip very easy. Sparkfun has a great tutorial on how to program the chip with the AVR programmer right [here](https://learn.sparkfun.com/tutorials/tiny-avr-programmer-hookup-guide/all).

## The ATtiny85

This section will give a brief overview of the ATtiny85 microcontroller and the important registers which we utilize to make the chip function.

`DDRB`

This is the data direction register for `PORTB`, which is the only port on the microcontroller. This register allows for the selection of inputs or outputs on the data pins. Setting a `1` on a bit will make that pin an output. Currently, `PB1` is set as an output with the line `DDRB = (1<<DDB1)`. This output represents the USI `data out` line.

`USICR`

The universal serial interface control register. Universal Serial Interface (USI) is a protocol that, in our case, allows for byte by byte transfer very similar to SPI. However, USI can be configured to mimic almost any serial interface, even USB. Currently, we utilize this register to set the wire mode of the bus to three-wire mode. This  means there will be a `data in` line, a `data out` line, and a `clock` line, which mimics SPI. We also use the register to set the `clock` line to be an external, positive edge triggered clock. This means that our master device supplies the clock and bits will be read on the positive edge of the clock. These are set by the line `USICR = (1<<USIWM0)|(1<<USICS1)`. From this register we can also enable USI overflow interrupts via the `USIOIE` bit of this register. This interrupt will fire on the 16th clock cycle as this is an overflow for a 4-bit counter.

`USIDR`

This is the data register for the universal serial interface. The data in the register works both ways meaning when the master sends a byte to us, this register will contain that data. We can also set this register to any byte we want and the ATtiny will shift out the bits on the next available clock cycles.

`ADCSRA`

This register controls the ADC of the microcontroller. It is a 10 bit ADC, which brings up some complications discussed later. From here, we enable the ADC and select which prescaler we wish to use for the ADC. The prescaler value is a 3-bit number and defines the clock frequency of the ADC. This frequency will be the main clock frequency (1 Mhz) devided by the prescaler value. Currently we use a prescaler value of 8 which means an ADC clock frequency of 125 khz. The ADC is enabled and the prescaler value is selected via the line `ADCSRA = (1<<ADEN)|(1<<ADPS1)|(0<<ADPS0)`. There is also a very important bit on this register named `ADSC`, which holds the status of the ADC. If we set this bit high, it will begin an analog read and will remain high as long as this read occurs.

`ADMUX`

This register allows us to select our analog input and select our internal reference voltage for the ADC. Selecting the input is multiplexed via the `MUX` bits. The internal reference voltage is selected via the `REFS` bits. Currently we select `PB4` as the analog in and we use VCC as the reference voltage.

`ADCL`

Because this is a 10-bit ADC, we need two registers to store this data. This register is the low bits of the 10-bit number.

`ADCH`

This register is the high bits of the 10-bit number read by the ADC. These numbers can be combined together and is discussed later.

`GIMSK`

The general interrupt mask register allows us to enable pin change interrupts globally. This is done by setting the `PCIE` bit of the register high.

`PCIMSK`

The pin change interrupt mask allows us to enable pin change interrupts on a specific pins. Currently, we use `PB3` as our chip select for SPI. Thus we can enable pin change interrupts on this pin by setting the `PCINT3` of this register high.

 `EECR`

 This is the EEPROM control register. We use EEPROM to hold a calibration constant indefinitely. The important bits on this register and how they are used are:
 1. `EEPE` - Set this bit high in order to execute a write to EEPROM. The bit will remain high as long as the write continues (they take a relatively long time). This is important because we must wait for previous writes to finish or we will corrupt the memory.
 2. `EEMPE` - Set this bit high in order to make setting `EEPE` high actually do anything.
 3. `EERE` - Set this bit high in order to put data in our selected address into the `EEDR` register.


 `EEAR`

 The EEPROM address register allows us to select which address we wish to read from or write to.

 `EEDR`

 The EEPROM data register allows us to select which data we wish to write into our selected address. On the other hand, it allows us to grab data from the specified address when executing an EEPROM read.

 ## Functionality

 `void write_calibration(uint8_t data)`

 This function will write the specified byte `data` into EEPROM. The address in EEPROM was chosen arbitrarily to be `0x10`.

 `uint8_t read_calibration()`

 This function will return the byte currently stored at memory address `0x10` in EEPROM.

 `union Float analog_read()`

 Firstly, `union Float` is very useful for transferring a `float` over SPI. A `union` allows for variables to share the same memory. In this case, we have made a float union in order to store both a `float` and a `uint8_t` array. Thus we have access to the `float` as well as the 4 bytes that make up the `float`. This allows us to send each byte of the `float` over SPI and then piece them back together on the master. This is done in the line:
 ```
 union Float {
  float val;
  uint8_t bytes[4];
};
 ```
 This function will commence an analog read and return the 10-bit number as a `float` encapsulated in a union. It actually does 10 analog reads and averages each value to ensure an accurate reading. Note that for reading `ADCL` and `ADCH`, it must be done in this specific order. If we were to change line 91 to `adc_avg.val += (ADCH<<8) | ADCL` then it would break the function. `ADCL` MUST be read before `ADCH`.

 `void do_overflow_interrupt()`

 This function should live in the Interrupt Service Routine (ISR) for the `USI_OVF_vect`. This interrupt fires when the USI clock counter overflows. See `USICR` for more info on the overflow interrupt. The function will grab the sent over byte from the master and then process it accordingly. It is important to note that we can only send a byte back as we receive a new byte. I will go through the timing of the `GET_LIGHT` command (`0x01`) which does an analog read and sends the float back over to the master byte by byte.
 1. if our `pos` variable is zero, it means that this is a new request. Thus perform an analog read and store the result in the `volatile union Float previous_read` variable. We then set `USIDR` to the first byte of our analog read. It's important to note that this does NOT instantly send over the byte to the master. We must wait for another byte to be sent over so that we can piggy back off of the master clock.
 2. On the next byte sent by the master, our first byte of the `float` will be sent to the master. We then prepare the second byte to be sent over.
 3. This repeats until all 4 bytes have been transferred to the master. So, in order to send 4 bytes to the master, the master must send 5 `GET_LIGHT` commands to the slave.


 `void do_pin_change_interrupt()`

 This function should live in the ISR for the `PCINT0_vect`. I know what you are thinking, shouldn't it be `PCINT3_vect` because that is what we set the interrupt on? Nope. There is actually only the `PCINT0_vect` which fires for all pin change interrupts no matter what pin it is set on. This interrupt will check the state of our pin 3 and if it is low, which signifies a transfer from the master is starting, then we enable the `USIOIE` flag. This allows for our USI overflow interrupt to fire so we can process SPI commands. If The pin state is high, then we disable USI overflow interrupts so that we do not accidentally process SPI commands for other devices sharing the line.

 