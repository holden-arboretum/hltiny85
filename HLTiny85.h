/*****************************************************************************
**  File name: HLTiny85.h                                                    *
**  Description: Driver for optical sensor board                             *
**  Notes:                                                                   *
**  Author(s): Humphrey, Dylan; Lapetina, Elyh                               *
**  Created: 3/26/2019                                                       *
**  Last Modified: 3/26/2019                                                 *
**  Changes:                                                                 *
******************************************************************************/

#ifndef HLTINY85_H
#define HLTINY85_H

#include <avr/interrupt.h>
#include <avr/io.h>

union Float {
  float val;
  uint8_t bytes[4];
};

class HLTiny85 {
public:
  HLTiny85();
  void do_overflow_interrupt();
  void do_pin_change_interrupt();
private:
  volatile uint8_t pos;
  volatile union Float previous_read;
  volatile bool got_eeprom_write;
  uint8_t read_calibration();
  void write_calibration(uint8_t data);
  union Float analog_read();
};

#endif HLTINY85_H
